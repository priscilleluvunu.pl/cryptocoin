import React from 'react';
import { PanelBody } from '@wordpress/components';
import { InspectorControls, URLInput, PanelColorSettings, ColorPalette } from '@wordpress/block-editor';


export function Inspector({attributes={}, setAttributes}){

    return(
        <React.Fragment>
            <InspectorControls>
                <PanelColorSettings
                    title="Couleurs de block"
                    colorSettings={[
                        {
                            label:"Couleur d'arrière-plan'",
                            value:attributes.background,
                            onChange:(background)=>setAttributes({background})
                        },
                        
                        {
                            label:"Couleur de CTA",
                            value:attributes.color,
                            onChange:(color)=>setAttributes({color})
                        }
                    ]}
                />
                <PanelBody title='CTA' opened>
                    <URLInput
                        value={attributes.url}
                        onChange={url=>setAttributes({url})}
                        isFullWidth
                    />
                </PanelBody>
            </InspectorControls>
        </React.Fragment>
    )

}