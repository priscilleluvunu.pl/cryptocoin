import React from 'react';
import { useBlockProps, RichText, InnerBlocks } from '@wordpress/block-editor';


export function Save(props){

    const { attributes } = props;

    return (
        <div { ...useBlockProps.save() } className="presentation" style={{ background:attributes.background }}>
           
           <div className='intro'>

                    <RichText.Content
                        value={attributes.title}
                        tagName="h1"
                    />  

                    <RichText.Content
                        value={attributes.sousTitle}
                        tagName="h2"
                    />

                    <InnerBlocks.Content/>

            </div>

        </div>
    );
}