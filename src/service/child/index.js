import { registerBlockType } from '@wordpress/blocks';
import { Edit } from './edit';
import { Save } from './save';
import metadata from './block.json';

registerBlockType( metadata.name, {
    save:Save,
    edit:Edit,
    name: metadata.name,
    title:metadata.title,
    "attributes": metadata.attributes,
    parent:metadata.parent,
})