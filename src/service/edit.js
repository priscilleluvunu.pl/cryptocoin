import React from 'react';
import { useBlockProps, BlockControls, RichText, InnerBlocks } from '@wordpress/block-editor';
import { Inspector } from './inspector';
import { Button, Placeholder } from '@wordpress/components';
import './editor.scss';

export function Edit(props){

    const blockProps = useBlockProps();

    const { setAttributes, attributes } = props;
    console.log("ATTRS", attributes);

    function removeImg(){
        setAttributes({
            image:undefined,
            imageUrl:undefined,
            alt:undefined
        })
    }

    return(
        <div {...blockProps }>  



           
            <div style={{ alignItems:"center", background:`${attributes.background}` }}>

            <div>

                <RichText
                    value={attributes.title}
                        onChange={(title)=>setAttributes({
                            title
                    })}
                    placeholder="Veuillez saisir le titre principal ici"
                    tagName='h1'
                />

                <RichText
                    value={attributes.sousTitle}
                        onChange={(sousTitle)=>setAttributes({
                            sousTitle
                    })}
                    placeholder="Veuillez saisir le sous titre ici"
                    tagName='h2'
                />

                <InnerBlocks
                    allowedBlocks={["cryptocoin/service-child"]}
                    renderAppender={InnerBlocks.ButtonBlockAppender}
                 />

            </div>
            </div>
        </div>
    )
}