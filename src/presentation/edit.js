import React from 'react';
import { useBlockProps, BlockControls, RichText, MediaUpload, MediaUploadCheck, MediaPlaceholder } from '@wordpress/block-editor';
import { Inspector } from './inspector';
import { Button, Placeholder } from '@wordpress/components';
import './editor.scss';

export function Edit(props){

    const blockProps = useBlockProps();

    const { setAttributes, attributes } = props;
    console.log("ATTRS", attributes);

    function removeImg(){
        setAttributes({
            image:undefined,
            imageUrl:undefined,
            alt:undefined
        })
    }

    return(
        <div {...blockProps }>
            <Inspector attributes={attributes} setAttributes={setAttributes} />
            <BlockControls>
                <Button onClick={removeImg}>
                    {`Supprimer l'image`}
                </Button>
            </BlockControls>    



           
            <div style={{ alignItems:"center", background:`${attributes.background}` }}>

            <div>

                <RichText
                    value={attributes.title}
                        onChange={(title)=>setAttributes({
                            title
                    })}
                    placeholder="Veuillez saisir le titre principal ici"
                    tagName='h1'
                />

                <RichText
                    value={attributes.sousTitle}
                        onChange={(sousTitle)=>setAttributes({
                            sousTitle
                    })}
                    placeholder="Veuillez saisir le sous titre ici"
                    tagName='h2'
                />

            </div>

                <div style={{minWidth:200 }}>
                    {!attributes.imageUrl1 ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image1:img?.id,
                                imageUrl1:img?.url,
                                alt1:img?.alt
                            })}
                            value={attributes.image1}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl1} />
                    )}

                <div>

                    <RichText
                        value={attributes.titre1}
                            onChange={(titre1)=>setAttributes({
                                titre1
                        })}
                        placeholder="Veuillez saisir le titre ici"
                        tagName='h3'
                    />

                    <RichText
                        value={attributes.description1}
                            onChange={(description1)=>setAttributes({
                                description1
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                    
                </div>
                </div>

                <div style={{minWidth:200 }}>
                    {!attributes.imageUrl2? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image2:img?.id,
                                imageUrl2:img?.url,
                                alt2:img?.alt
                            })}
                            value={attributes.image2}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl2} />
                    )}
                    
                    <div>

                    <RichText
                        value={attributes.titre2}
                            onChange={(titre2)=>setAttributes({
                                titre2
                        })}
                        placeholder="Veuillez saisir le titre ici"
                        tagName='h3'
                    />
                    <RichText
                        value={attributes.description2}
                            onChange={(description2)=>setAttributes({
                                description2
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                     </div>

                </div>

                <div style={{ minWidth:200 }}>
                    {!attributes.imageUrl3 ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image3:img?.id,
                                imageUrl3:img?.url,
                                alt3:img?.alt
                            })}
                            value={attributes.image3}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl3} />
                    )}

                <div>

                    <RichText
                        value={attributes.titre3}
                            onChange={(titre3)=>setAttributes({
                                titre3
                        })}
                        placeholder="Veuillez saisir le titre ici"
                        tagName='h3'
                    />
                    <RichText
                        value={attributes.description3}
                            onChange={(description3)=>setAttributes({
                                description3
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                </div>
                </div>  

                <div style={{minWidth:200 }}>
                    {!attributes.imageUrl4 ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image4:img?.id,
                                imageUrl4:img?.url,
                                alt4:img?.alt
                            })}
                            value={attributes.image4}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl4} />
                    )}

                <div>

                    <RichText
                        value={attributes.titre4}
                            onChange={(titre4)=>setAttributes({
                                titre4
                        })}
                        placeholder="Veuillez saisir le titre ici"
                        tagName='h3'
                    />

                    <RichText
                        value={attributes.description4}
                            onChange={(description4)=>setAttributes({
                                description4
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                    
                </div>
                </div>

                <div style={{minWidth:200 }}>
                    {!attributes.imageUrl5 ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image5:img?.id,
                                imageUrl5:img?.url,
                                alt5:img?.alt
                            })}
                            value={attributes.image5}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl5} />
                    )}

                <div>

                    <RichText
                        value={attributes.titre5}
                            onChange={(titre5)=>setAttributes({
                                titre5
                        })}
                        placeholder="Veuillez saisir le titre ici"
                        tagName='h3'
                    />

                    <RichText
                        value={attributes.description5}
                            onChange={(description5)=>setAttributes({
                                description5
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                    
                </div>
                </div>

                <div style={{minWidth:200 }}>
                    {!attributes.imageUrl6 ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image6:img?.id,
                                imageUrl6:img?.url,
                                alt6:img?.alt
                            })}
                            value={attributes.image6}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl6} />
                    )}

                <div>

                    <RichText
                        value={attributes.titre6}
                            onChange={(titre6)=>setAttributes({
                                titre6
                        })}
                        placeholder="Veuillez saisir le titre ici"
                        tagName='h3'
                    />

                    <RichText
                        value={attributes.description6}
                            onChange={(description6)=>setAttributes({
                                description6
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                    
                </div>
                </div>
                
            </div>
        </div>
    )
}