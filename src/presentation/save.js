import React from 'react';
import { useBlockProps, RichText } from '@wordpress/block-editor';


export function Save(props){

    const { attributes } = props;

    return (
        <div { ...useBlockProps.save() } className="presentation" style={{ background:attributes.background }}>
           
           <div className='intro'>

                    <RichText.Content
                        value={attributes.title}
                        tagName="h1"
                    />  

                    <RichText.Content
                        value={attributes.sousTitle}
                        tagName="h2"
                    />
            </div>

            <div className='block_1' >
                <div className='presentation__image'>
                        <img src={attributes.imageUrl1} alt={attributes.alt1} />
                        
                    <div className='presentation__content'>

                        <RichText.Content
                            value={attributes.titre1}
                            tagName="h3"
                        />  
                        <RichText.Content
                            value={attributes.description1}
                            tagName="p"
                        />

                    </div>
                </div>

                <div className='presentation__image2'>
                    <img src={attributes.imageUrl2} alt={attributes.alt2} />

                    <div className='presentation__content2'>

                        <RichText.Content
                            value={attributes.titre2}
                            tagName="h3"
                        />  
                        
                        <RichText.Content
                            value={attributes.description2}
                            tagName="p"
                        />

                    </div>
                </div>

                <div className='presentation__image3'>
                    <img src={attributes.imageUrl3} alt={attributes.alt3} />

                    <div className='presentation__content3'>

                    <RichText.Content
                        value={attributes.titre3}
                        tagName="h3"
                    />  
                    <RichText.Content
                        value={attributes.description3}
                        tagName="p"
                    />

                    </div>
                </div>
            </div>


        <div className='block_2'>
            <div className='presentation__image4'>
                <img src={attributes.imageUrl4} alt={attributes.alt4} />
                
                <div className='presentation__content4'>

                 <RichText.Content
                    value={attributes.titre4}
                    tagName="h3"
                />  
                <RichText.Content
                    value={attributes.description4}
                    tagName="p"
                />

                </div>
            </div>

            <div className='presentation__image5'>
                <img src={attributes.imageUrl5} alt={attributes.alt5} />

                <div className='presentation__content5'>

                    <RichText.Content
                        value={attributes.titre5}
                        tagName="h3"
                    />  
                    
                    <RichText.Content
                        value={attributes.description5}
                        tagName="p"
                    />

                </div>
            </div>

            <div className='presentation__image6'>
                <img src={attributes.imageUrl6} alt={attributes.alt6} />

                <div className='presentation__content6'>

                 <RichText.Content
                    value={attributes.titre6}
                    tagName="h3"
                />  
                <RichText.Content
                    value={attributes.description6}
                    tagName="p"
                />

                </div>
            </div>
        </div>

        </div>
    );
}