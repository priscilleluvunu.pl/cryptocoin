import React from 'react';
import { useBlockProps, BlockControls, RichText, MediaUpload, MediaUploadCheck, MediaPlaceholder } from '@wordpress/block-editor';
import { Inspector } from './inspector';
import { Button, Placeholder } from '@wordpress/components';
import './editor.scss';

export function Edit(props){

    const blockProps = useBlockProps();

    const { setAttributes, attributes } = props;
    console.log("ATTRS", attributes);

    function removeImg(){
        setAttributes({
            image:undefined,
            imageUrl:undefined,
            alt:undefined
        })
    }

    return(
        <div {...blockProps }>
            <Inspector attributes={attributes} setAttributes={setAttributes} />
            <BlockControls>
                <Button onClick={removeImg}>
                    {`Supprimer l'image`}
                </Button>
            </BlockControls>    

           
            <div style={{ alignItems:"center", background:`${attributes.background}` }}>

                <div style={{minWidth:200 }}>
                    {!attributes.imageUrl1 ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image1:img?.id,
                                imageUrl1:img?.url,
                                alt1:img?.alt
                            })}
                            value={attributes.image1}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl1} />
                    )}

                <div>

                    <RichText
                        value={attributes.numero}
                            onChange={(numero)=>setAttributes({
                                numero
                        })}
                        placeholder="Veuillez saisir le numero ici"
                        tagName='span'
                    />

                    <RichText
                        value={attributes.description1}
                            onChange={(description1)=>setAttributes({
                                description1
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                    
                </div>
            </div>

                <div style={{minWidth:200 }}>
                    {!attributes.imageUrl2? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image2:img?.id,
                                imageUrl2:img?.url,
                                alt2:img?.alt
                            })}
                            value={attributes.image2}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl2} />
                    )}
                    
                    <div>

                    <RichText
                        value={attributes.numero}
                            onChange={(numero)=>setAttributes({
                                numero
                        })}
                        placeholder="Veuillez saisir le numero ici"
                        tagName='span'
                    />
                    <RichText
                        value={attributes.description2}
                            onChange={(description2)=>setAttributes({
                                description2
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                     </div>

                </div>

                <div style={{ minWidth:200 }}>
                    {!attributes.imageUrl3 ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image3:img?.id,
                                imageUrl3:img?.url,
                                alt3:img?.alt
                            })}
                            value={attributes.image3}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl3} />
                    )}

                <div>

                    <RichText
                        value={attributes.numero}
                            onChange={(numero)=>setAttributes({
                                numero
                        })}
                        placeholder="Veuillez saisir le numero ici"
                        tagName='span'
                    />
                    <RichText
                        value={attributes.description3}
                            onChange={(description3)=>setAttributes({
                                description3
                        })}
                        placeholder="Veuillez saisir la description ici"
                        tagName='p'
                    />
                   
                </div>
            </div>  
            </div>
        </div>
    )
}