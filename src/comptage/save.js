import React from 'react';
import { useBlockProps, RichText } from '@wordpress/block-editor';


export function Save(props){

    const { attributes } = props;

    return (
        <div { ...useBlockProps.save() } className="comptage" style={{ background:attributes.background }}>
           
           <div className='comptage__image'>
                <img src={attributes.imageUrl1} alt={attributes.alt1} />
                
                <div className='comptage__content'>

                 <RichText.Content
                    value={attributes.numero}
                    tagName="span"
                />  
                <RichText.Content
                    value={attributes.description1}
                    tagName="p"
                />

                </div>
            </div>

            <div className='comptage__image2'>
                <img src={attributes.imageUrl2} alt={attributes.alt2} />

                <div className='comptage__content2'>

                    <RichText.Content
                        value={attributes.numero}
                        tagName="span"
                    />  
                    
                    <RichText.Content
                        value={attributes.description2}
                        tagName="p"
                    />

                </div>
            </div>

            <div className='comptage__image3'>
                <img src={attributes.imageUrl3} alt={attributes.alt3} />

                <div className='comptage__content3'>

                 <RichText.Content
                    value={attributes.numero}
                    tagName="span"
                />  
                <RichText.Content
                    value={attributes.description3}
                    tagName="p"
                />

                </div>
            </div>

        </div>
    );
}