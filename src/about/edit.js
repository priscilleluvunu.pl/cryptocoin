import React from 'react';
import { useBlockProps, BlockControls, RichText, MediaUpload, MediaUploadCheck, MediaPlaceholder } from '@wordpress/block-editor';
import { Inspector } from './inspector';
import { Button, Placeholder } from '@wordpress/components';
import './editor.scss';

export function Edit(props){

    const blockProps = useBlockProps();

    const { setAttributes, attributes } = props;
    console.log("ATTRS", attributes);

    function removeImg(){
        setAttributes({
            image:undefined,
            imageUrl:undefined,
            alt:undefined
        })
    }

    return(
        <div {...blockProps }>
            <Inspector attributes={attributes} setAttributes={setAttributes} />
            <BlockControls>
                <Button onClick={removeImg}>
                    {`Supprimer l'image`}
                </Button>
            </BlockControls>    
           
            <div style={{ flex:1 , alignItems:"center", background:`${attributes.background}` }}>

                <div style={{display: 'inline-block', minWidth:200 }}>
                    {!attributes.imageUrl ? (<MediaUploadCheck>
                        <MediaUpload
                            onSelect={img=>setAttributes({
                                image:img?.id,
                                imageUrl:img?.url,
                                alt:img?.alt
                            })}
                            value={attributes.image}
                            render={({open})=>(
                                <Placeholder  
                                    icon="image-filter"
                                    label='Image'
                                    instructions='tala '
                                >
                                    <Button
                                        onClick={open}
                                        icon="upload"
                                        isSmall
                                    >
                                        {`Ajouter`}
                                    </Button>
                                </Placeholder>
                            )}
                        />
                    </MediaUploadCheck>) :(
                        <img src={attributes.imageUrl} />
                    )}
                </div>

                <div style={{ display: 'flex', flexDirection:'column' }}>
                    <RichText
                        value={attributes.title}
                            onChange={(title)=>setAttributes({
                                title
                        })}
                        placeholder="Veuillez saisir le titre principal ici"
                        tagName='h1'
                    />
                    <RichText
                        value={attributes.sousTitle}
                            onChange={(sousTitle)=>setAttributes({
                                sousTitle
                        })}
                        placeholder="Veuillez saisir le sous-titre ici"
                        tagName='h2'
                    />
                    <RichText
                        value={attributes.description}
                        onChange={(description)=>setAttributes({
                            description
                        })}
                        placeholder="Veuillez saisir la déscription ici"
                        tagName='p'
                    />
                    <button style={{ background:attributes.color }} type='button'>
                        <RichText
                            value={attributes.label}
                            onChange={(label)=>setAttributes({
                                label
                            })}
                            placeholder="label du bouton"
                            tagName='span'
                        />  
                    </button>
                </div>
                
            </div>
        </div>
    )
}